#include<stdio.h>
#include<unistd.h>
#include<string.h>

/* Sets numbers that represent the players. */
#define P1 -1
#define P2 1
#define P1_king -2
#define P2_king 2

short int board[8][8] = {
    {0,P2,0,P2,0,P2,0,P2},
    {P2,0,P2,0,P2,0,P2,0},
    {0,P2,0,P2,0,P2,0,P2},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {P1,0,P1,0,P1,0,P1,0},
    {0,P1,0,P1,0,P1,0,P1},
    {P1,0,P1,0,P1,0,P1,0}
};

unsigned short int score[4] = {12,12,12,12};

struct piece {
    unsigned short int x,y;
};

void init() {
    setbuf(stdout, NULL);
    printf("\033[2J\033[0;0HPlayer 1 is \x1B[1;30mBLACK\x1B[0m\nPlayer 2 is \x1B[1;37mWHITE\x1B[0m\nPlayer 1 starts!\n\nPress [Enter] to continue...");
    while(!getchar()) {}
}

void boardEvalCheckers() {
    /* Iterates over the board and counts pieces of each type. */
    short int xi,yi;
    memset(&score, 0, sizeof(score));
    for(yi=7; yi >= 0; yi--) {
        for(xi=0; xi < 8; xi++) {
            switch(board[yi][xi]) {
                case P1:
                    ++score[0];
                    break;
                case P2:
                    ++score[1];
                    break;
            }
        }
    }
}

void drawBoard() {
    short int xi,yi,si1,si2;
    printf("\033[2J\033[0;0H  ________ ");
    for(si2 = (12 - score[1]); si2 > 0; si2--) /* Places captured White pieces next to Black's side of the board */
        printf("\x1B[1;37mO\x1B[0m");
    printf("\n");
    for(yi=7; yi>=0; yi--) {
        printf("%d|", yi);
        for(xi=0; xi<8; xi++) {
          switch(board[yi][xi]) { /* Core board display logic. The switch operates on each part of the board array and prints something based on it */
              case 0:
                  printf(!((xi + yi) % 2) ? " " : "#");
                  break;
              case P1:
                  printf("\x1B[1;30mO\x1B[0m");
                  break;
              case P2:
                  printf("\x1B[1;37mO\x1B[0m");
                  break;
              case P1_king:
                  printf("\x1B[1;30m@\x1B[0m");
                  break;
              case P2_king:
                  printf("\x1B[1;37m@\x1B[0m");
                  break;
          }
        }
        printf("\n");
    }
    printf("  01234567 ");
    for(si1 = (12 - score[0]); si1 > 0; si1--) /* Places captured Black pieces next to White's side of the board */
        printf("\x1B[1;30mO\x1B[0m");
    printf("\n");
}

/* Weird-ass function necessary to "eat" characters left unprocessed by fscanf in the input buffer. */
void clearBuf() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) { }
}

int checkPosCheckers(unsigned short int x, unsigned short int y) {
    if(x > 7)
        return(1);
    if(y > 7)
        return(1);
    if(!((x + y) % 2))
        return(1);
    return 0;
}

unsigned int getPlayer() {
    unsigned short int P = 0;
    while(P == 0) {
        P = 0;
        printf("Select Player: ");
        while((scanf("%hu",&P) != 1)) {
        printf("Invalid entry!\n");
        clearBuf();
        }
        switch(P) {
            case 1:
                break;
            case 2:
                break;
            default:
                P = 0;
                printf("Invalid player!\n");
        }
    }
    return P;
}

int checkOwner(unsigned short int x, unsigned short int y, unsigned short int player) {
    short int piece = board[y][x];
    switch(player) {
        case 1:
            return(!(piece < 0));
        case 2:
            return(!(piece > 0));
        default:
            return 2;

    }
}

/* This function makes sure a move is valid in checkers based on the rules. Since valid spaces are checkered, X with always be +- 1, and Y will increase or decrease depending on team. */
int checkMoveCheckers(struct piece start, struct piece dest){
    if(board[start.y][start.x] == board[dest.y][dest.x])
    {printf("Same piece\t");return 1;}
    switch(board[start.y][start.x]) {
        case -1:
            if((dest.x != start.x+1 && dest.x != start.x-1) || dest.y != start.y-1)
                return 1;
            else
                return 0;
        case 1:
            if((dest.x != start.x+1 && dest.x != start.x-1) || dest.y != start.y+1)
                return 1;
            else
                return 0;
        default:
            return 2;
    }
}

/* Takes coordinates from player input and runs validity checks. */
struct piece getCoords() {
    unsigned short int X,Y;
    struct piece out;
    while((scanf("%hu,%hu", &X, &Y) != 2) || (checkPosCheckers(X, Y) != 0)) {
        printf("Invalid coordinates!\n");
        clearBuf();
    }
    out.x = X; out.y = Y;
    return out;
}

/* Takes a player input, piece input, and destination input. Runs checks and moves a piece if the inputs are valid. No analysis of the move's effects are needed—that's handled by the boardEval function. */
void move() {
    struct piece curr,dest;

    unsigned short int player = getPlayer();
    printf("Player %d selected!\n", player);

    while(1) {
       printf("Select a piece: ");
       curr = getCoords();

       if(checkOwner(curr.x, curr.y, player) != 0) {
           printf("You do not own a piece at that position!\n");
           continue;
       }
       printf("Select destination: ");
       dest = getCoords();
       if(checkMoveCheckers(curr, dest)) {
            printf("This is not a valid move!\n");
            clearBuf();
            continue;
       }
       board[dest.y][dest.x] = board[curr.y][curr.x];
       board[curr.y][curr.x] = 0;
       printf("Moved!\n"),
       sleep(1);
       break;
   }
}

int main() {
    init();
    while(1) {
        boardEvalCheckers();
        drawBoard();
        move();
    }
    return 0;
}
