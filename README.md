# CChessckers

An extensible board game engine written in C89.

# Progress
- [x] Playable checkers
- [x] Scorekeeping
- [x] Score display
- [ ] Game end detection
- [ ] Game type system
- [ ] Playable chess
- [ ] Games with >2 players

# Possible expansions
At some point, the project may be expanded to use X11/OpenGL.
